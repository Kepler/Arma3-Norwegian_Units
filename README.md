Norwegian Units Modification for Arma 3
=======================================

Forked from [Vasilyevich on Github](https://github.com/Vasilyevich/Norwegian_Units)

http://forums.bistudio.com/showthread.php?179995-Norwegian-Units

<a rel="license" href="http://www.bistudio.com/licenses/arma-public-license-share-alike" target="_blank" >
   <img src="http://www.bistudio.com/license-icons/small/APL-SA.png" >
   <br>
   This work is licensed under a Arma Public License Share Alike
</a>


