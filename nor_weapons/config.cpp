class CfgPatches
{
	class NOR_Weapons
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
	};
};
class cfgWeapons
{
	class CUP_lmg_m249_SQuantoon;
	class CUP_lmg_m249_pip1;
	class CUP_lmg_m249_pip4;
	class CUP_arifle_Mk16_STD;
	class CUP_arifle_Mk16_STD_black;
	class CUP_arifle_Mk16_STD_woodland;
	class CUP_arifle_Mk16_STD_EGLM;
	class CUP_arifle_Mk16_STD_EGLM_black;
	class CUP_arifle_Mk16_STD_EGLM_woodland;
	class CUP_arifle_Mk16_CQC;
	class CUP_arifle_Mk16_CQC_black;
	class CUP_arifle_Mk16_CQC_woodland;
	class CUP_arifle_Mk16_CQC_EGLM;
	class CUP_arifle_Mk16_CQC_EGLM_black;
	class CUP_arifle_Mk16_CQC_EGLM_woodland;
	class CUP_arifle_Mk17_STD;
	class CUP_arifle_Mk17_STD_black;
	class CUP_arifle_Mk17_STD_woodland;
	class CUP_srifle_M107_Base;
	class CUP_hgun_Glock17;

	class NOR_M249_ACO_IR: CUP_lmg_m249_SQuantoon
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Aco";
				slot = "CowsSlot";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "PointerSlot";
			};
		};
	};
	class NOR_M249_ACO_IR_Desert: CUP_lmg_m249_pip1
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Aco";
				slot = "CowsSlot";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "PointerSlot";
			};
		};
	};
	class NOR_M249_ACO_IR_Wood: CUP_lmg_m249_pip4
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Aco";
				slot = "CowsSlot";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "PointerSlot";
			};
		};
	};
	class NOR_C8_UGL_RCO_IR_S: CUP_arifle_Mk16_STD_EGLM_black
	{
		scope = 1;
		scopeCurator = 1;
		displayName = "C8 SFW m203";
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_C8_UGL_RCO_IR_S_Desert: CUP_arifle_Mk16_STD_EGLM
	{
		scope = 1;
		scopeCurator = 1;
		displayName = "C8 SFW m203";
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_C8_UGL_RCO_IR_S_Wood: CUP_arifle_Mk16_STD_EGLM_woodland
	{
		scope = 1;
		scopeCurator = 1;
		displayName = "C8 SFW m203";
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_C8_MRCO_IR_S: CUP_arifle_Mk16_STD_black
	{
		scope = 1;
		scopeCurator = 1;
		displayName = "C8 SFW";
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_MRCO";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_C8_MRCO_IR_S_Desert: CUP_arifle_Mk16_STD
	{
		scope = 1;
		scopeCurator = 1;
		displayName = "C8 SFW";
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_MRCO";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_C8_MRCO_IR_S_Wood: CUP_arifle_Mk16_STD_woodland
	{
		scope = 1;
		scopeCurator = 1;
		displayName = "C8 SFW";
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_MRCO";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_Barrett_M82_LRPS: CUP_srifle_M107_Base
	{
		scope = 1;
		scopeCurator = 1;
		displayName = "Barrett M82";
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_LRPS";
				slot = "CowsSlot";
			};
		};
	};
	class NOR_Barrett_M82_LRPS_Wood: CUP_srifle_M107_Base
	{
		scope = 1;
		scopeCurator = 1;
		displayName = "Barrett M82";
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_LRPS";
				slot = "CowsSlot";
			};
		};
	};
	class NOR_Barrett_M82_LRPS_Desert: CUP_srifle_M107_Base
	{
		scope = 1;
		scopeCurator = 1;
		displayName = "Barrett M82";
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_LRPS";
				slot = "CowsSlot";
			};
		};
	};
	class NOR_HK416c_MRCO_IR_S: CUP_arifle_Mk16_CQC_black
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_MRCO";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416c_MRCO_IR_S_Desert: CUP_arifle_Mk16_CQC
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_MRCO";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416c_MRCO_IR_S_Wood: CUP_arifle_Mk16_CQC_woodland
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_MRCO";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416c_UGL_RCO_IR_S: CUP_arifle_Mk16_CQC_EGLM_black
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416c_UGL_RCO_IR_S_Desert: CUP_arifle_Mk16_CQC_EGLM
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416c_UGL_RCO_IR_S_Wood: CUP_arifle_Mk16_CQC_EGLM_woodland
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK417_TVS_IR_S: CUP_arifle_Mk17_STD_black
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_tws";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK417_TVS_IR_S_Desert: CUP_arifle_Mk17_STD
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_tws";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK417_TVS_IR_S_Wood: CUP_arifle_Mk17_STD_woodland
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_tws";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK417_DMS_IR: CUP_arifle_Mk17_STD_black
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_DMS";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
			class LinkedItemsUnder
			{
				item = "bipod_01_F_blk";
				slot = "CUP_PicatinnyUnderMountSCAR";
			};
		};
	};
	class NOR_HK417_DMS_IR_Desert: CUP_arifle_Mk17_STD
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_DMS";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
			class LinkedItemsUnder
			{
				item = "bipod_01_F_snd";
				slot = "CUP_PicatinnyUnderMountSCAR";
			};
		};
	};
	class NOR_HK417_DMS_IR_Wood: CUP_arifle_Mk17_STD_woodland
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_DMS";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
			class LinkedItemsUnder
			{
				item = "bipod_01_F_mtp";
				slot = "CUP_PicatinnyUnderMountSCAR";
			};
		};
	};
	class NOR_HK416_ACO_IR: CUP_arifle_Mk16_STD_black
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416_ACO_IR_Desert: CUP_arifle_Mk16_STD
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416_ACO_IR_Wood: CUP_arifle_Mk16_STD_woodland
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416_UGL_RCO_IR: CUP_arifle_Mk16_STD_EGLM_black
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416_UGL_RCO_IR_Desert: CUP_arifle_Mk16_STD_EGLM
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_HK416_UGL_RCO_IR_Wood: CUP_arifle_Mk16_STD_EGLM_woodland
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "optic_Hamr";
				slot = "CUP_PicatinnyTopMountSCAR";
			};
			class LinkedItemsAcc
			{
				item = "acc_pointer_IR";
				slot = "CUP_PicatinnySideMountSCAR";
			};
		};
	};
	class NOR_Glock_17_S: CUP_hgun_Glock17
	{
		scope = 1;
		scopeCurator = 1;
		class LinkedItems
		{
			class LinkedItemsMuzzle
			{
				item = "muzzle_snds_L";
				slot = "MuzzleSlot";
			};
		};
	};
};
