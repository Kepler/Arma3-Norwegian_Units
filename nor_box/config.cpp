class CfgPatches {
    class Norwegian_retexture {
        units[] = {"NOR_Box_Everything", "NOR_Box_Ammo", "NOR_Box_WeaponsBasic", "NOR_Box_WeaponsSpecial"};
        weapons[] = {};
        requiredVersion = 0.1;
	    requiredAddons[] = {"NOR_Equip"};
    };
};
class CfgVehicles {
	class B_supplyCrate_F;
	class NOR_Box_Everything: B_supplyCrate_F
	{
		author = "Vasilyevich";
		scopeCurator = 2;
		displayName = "Supply Box [NOR]";
		transportmaxmagazines = 99999;
		transportmaxweapons = 9999;
		transportmaxbackpacks = 99;
		maximumload = 999999;
		class TransportItems
		{
			class _xx_NOR_Carrier_Standard_2_Green
			{
				count = 5;
				name = "NOR_Carrier_Standard_2_Green";
			};
			class _xx_NOR_Carrier_Standard_1_Green
			{
				count = 5;
				name = "NOR_Carrier_Standard_1_Green";
			};
			class _xx_NOR_Carrier_Standard_2_Navy
			{
				count = 5;
				name = "NOR_Carrier_Standard_2_Navy";
			};
			class _xx_NOR_Carrier_Standard_1_Navy
			{
				count = 5;
				name = "NOR_Carrier_Standard_1_Navy";
			};
			class _xx_NOR_Carrier_Standard_2_Multi
			{
				count = 5;
				name = "NOR_Carrier_Standard_2_Multi";
			};
			class _xx_NOR_Carrier_Standard_1_Multi
			{
				count = 5;
				name = "NOR_Carrier_Standard_1_Multi";
			};
			class _xx_NOR_Carrier_Standard_2_Black
			{
				count = 5;
				name = "NOR_Carrier_Standard_2_Black";
			};
			class _xx_NOR_Carrier_Standard_1_Black
			{
				count = 5;
				name = "NOR_Carrier_Standard_1_Black";
			};
			class _xx_NOR_Carrier_Standard_2_Wood
			{
				count = 5;
				name = "NOR_Carrier_Standard_2_Wood";
			};
			class _xx_NOR_Carrier_Standard_1_Wood
			{
				count = 5;
				name = "NOR_Carrier_Standard_1_Wood";
			};
			class _xx_NOR_Carrier_Standard_2_Desert
			{
				count = 5;
				name = "NOR_Carrier_Standard_2_Desert";
			};
			class _xx_NOR_Carrier_Standard_1_Desert
			{
				count = 5;
				name = "NOR_Carrier_Standard_1_Desert";
			};
			class _xx_NOR_NVGoggles
			{
				count = 5;
				name = "NOR_NVGoggles";
			};
						class _xx_NOR_Helmet_Wood
			{
				count = 5;
				name = "NOR_Helmet_Wood";
			};
			class _xx_NOR_Helmet_Desert
			{
				count = 5;
				name = "NOR_Helmet_Desert";
			};
			class _xx_NOR_Beret_Officer
			{
				count = 5;
				name = "NOR_Beret_Officer";
			};
			class _xx_NOR_Booniehat_Wood
			{
				count = 5;
				name = "NOR_Booniehat_Wood";
			};
			class _xx_NOR_Booniehat_Desert
			{
				count = 5;
				name = "NOR_Booniehat_Desert";
			};
			class _xx_NOR_Helmet_ECH_Light_Desert
			{
				count = 5;
				name = "NOR_Helmet_ECH_Light_Desert";
			};
			class _xx_NOR_Helmet_ECH_Light_Grey
			{
				count = 5;
				name = "NOR_Helmet_ECH_Light_Grey";
			};
			class _xx_NOR_Helmet_ECH_Light_Multi
			{
				count = 5;
				name = "NOR_Helmet_ECH_Light_Multi";
			};
			class _xx_NOR_Helmet_ECH_Light_Black
			{
				count = 5;
				name = "NOR_Helmet_ECH_Light_Black";
			};
			class _xx_NOR_Helmet_ECH_Desert
			{
				count = 5;
				name = "NOR_Helmet_ECH_Desert";
			};
			class _xx_NOR_Helmet_ECH_Grey
			{
				count = 5;
				name = "NOR_Helmet_ECH_Grey";
			};
			class _xx_NOR_Helmet_ECH_Multi
			{
				count = 5;
				name = "NOR_Helmet_ECH_Multi";
			};
			class _xx_NOR_Helmet_ECH_Black
			{
				count = 5;
				name = "NOR_Helmet_ECH_Black";
			};
			class _xx_NOR_Cap_Headphones_Multi
			{
				count = 5;
				name = "NOR_Cap_Headphones_Multi";
			};
			class _xx_NOR_Cap_Headphones_Black
			{
				count = 5;
				name = "NOR_Cap_Headphones_Black";
			};
			class _xx_NOR_Cap_Headphones_Navy
			{
				count = 5;
				name = "NOR_Cap_Headphones_Navy";
			};
			class _xx_NOR_Cap_Headphones_Coyote
			{
				count = 5;
				name = "NOR_Cap_Headphones_Coyote";
			};
			class _xx_NOR_Cap_Headphones_Grey
			{
				count = 5;
				name = "NOR_Cap_Headphones_Grey";
			};
			class _xx_NOR_Uniform_MJK_Grey
			{
				count = 5;
				name = "NOR_Uniform_MJK_Grey";
			};
			class _xx_NOR_Uniform_Short_MJK_Grey
			{
				count = 5;
				name = "NOR_Uniform_Short_MJK_Grey";
			};
			class _xx_NOR_Uniform_MJK_Coyote
			{
				count = 5;
				name = "NOR_Uniform_MJK_Coyote";
			};
			class _xx_NOR_Uniform_Short_MJK_Coyote
			{
				count = 5;
				name = "NOR_Uniform_Short_MJK_Coyote";
			};
			class _xx_NOR_Uniform_FSK_Grey
			{
				count = 5;
				name = "NOR_Uniform_FSK_Grey";
			};
			class _xx_NOR_Uniform_Short_FSK_Grey
			{
				count = 5;
				name = "NOR_Uniform_Short_FSK_Grey";
			};
			class _xx_NOR_Uniform_FSK_Coyote
			{
				count = 5;
				name = "NOR_Uniform_FSK_Coyote";
			};
			class _xx_NOR_Uniform_Short_FSK_Coyote
			{
				count = 5;
				name = "NOR_Uniform_Short_FSK_Coyote";
			};
			class _xx_NOR_Uniform_FSK_Multi
			{
				count = 5;
				name = "NOR_Uniform_FSK_Multi";
			};
			class _xx_NOR_Uniform_FSK_Black
			{
				count = 5;
				name = "NOR_Uniform_FSK_Black";
			};
			class _xx_NOR_Uniform_MJK_Navy
			{
				count = 5;
				name = "NOR_Uniform_MJK_Navy";
			};
			class _xx_NOR_Uniform_Short_FSK_Multi
			{
				count = 5;
				name = "NOR_Uniform_Short_FSK_Multi";
			};
			class _xx_NOR_Uniform_Short_FSK_Black
			{
				count = 5;
				name = "NOR_Uniform_Short_FSK_Black";
			};
			class _xx_NOR_Uniform_Short_MJK_Navy
			{
				count = 5;
				name = "NOR_Uniform_Short_MJK_Navy";
			};
		};
		class TransportBackpacks
		{
		};
		class TransportMagazines
		{
		};
		class TransportWeapons
		{
		};
	};

	class NOR_Box_Ammo: B_supplyCrate_F
	{
		author = "Kepler";
		scopeCurator = 2;
		displayName = "Basic Ammo [NOR]";
		transportmaxmagazines = 99999;
		transportmaxweapons = 9999;
		transportmaxbackpacks = 99;
		maximumload = 999999;
		class TransportItems
		{
         class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 10;
			};
		};
		class TransportBackpacks
		{
		};
		class TransportMagazines
		{
         class _xx_200Rnd_mas_556x45_T_Stanag
			{
				magazine = "CUP_200Rnd_TE4_Red_Tracer_556x45_M249";
				count = 3;
			};
         class _xx_mas_MAAWS
			{
				magazine = "CUP_M136_M";
				count = 3;
			};
         class _xx_30Rnd_mas_556x45_Stanag
			{
				magazine = "CUP_30Rnd_556x45_Stanag";
				count = 20;
			};
         class _xx_CUP_20Rnd_762x51_B_SCAR
         {
            magazine = "CUP_20Rnd_762x51_B_SCAR";
            count = 20;
         }
			class _xx_30Rnd_556x45_Stanag_Tracer_Red
			{
				count = 10;
				magazine = "30Rnd_556x45_Stanag_Tracer_Red";
			};
         class _xx_1Rnd_HE_Grenade_shell
			{
				magazine = "1Rnd_HE_Grenade_shell";
				count = 10;
			};
         class _xx_UGL_FlareWhite_F
			{
				magazine = "UGL_FlareWhite_F";
				count = 5;
			};
			class _xx_1Rnd_Smoke_Grenade_shell
			{
				magazine = "1Rnd_Smoke_Grenade_shell";
				count = 5;
			};
         class _xx_16Rnd_9x21_Mag
         {
            magazine = "16Rnd_9x21_Mag";
            count = 10;
         };
         class _xx_B_IR_Grenade
         {
            magazine = "B_IR_Grenade";
            count = 10;
         };
         class _xx_HandGrenade
         {
            magazine = "HandGrenade";
            count = 10;
         };
         class _xx_SmokeShell
         {
            magazine = "SmokeShell";
            count = 10;
         };
		};
		class TransportWeapons
		{
		};
	};

   class NOR_Box_WeaponsBasic: B_supplyCrate_F
	{
		author = "Kepler";
		scopeCurator = 2;
		displayName = "Basic Weapons [NOR]";
		transportmaxmagazines = 99999;
		transportmaxweapons = 9999;
		transportmaxbackpacks = 99;
		maximumload = 999999;
		class TransportItems
		{
         class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 10;
			};
         class _xx_Optic_ACO
         {
            name = "optic_ACO";
            count = 8;
         };
		};
		class TransportBackpacks
		{
		};
		class TransportMagazines
		{
         class _xx_30Rnd_mas_556x45_Stanag
			{
				magazine = "CUP_30Rnd_556x45_Stanag";
				count = 30;
			};
         class _xx_16Rnd_9x21_Mag
         {
            magazine = "16Rnd_9x21_Mag";
            count = 16;
         };
      };
      class TransportWeapons
		{
         class _xx_CUP_arifle_Mk16_STD_black
         {
            weapon = "CUP_arifle_Mk16_STD_black";
            count = 8;
         };
         class _xx_NOR_Glock_17_S
         {
            weapon = "NOR_Glock_17_S";
            count = 8;
         };
		};
   };

   class NOR_Box_WeaponsSpecial: B_supplyCrate_F
	{
		author = "Kepler";
		scopeCurator = 2;
		displayName = "Special Weapons [NOR]";
		transportmaxmagazines = 99999;
		transportmaxweapons = 9999;
		transportmaxbackpacks = 99;
		maximumload = 999999;
		class TransportItems
		{
         class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 10;
			};
         class _xx_Optic_Hamr
         {
            name = "optic_Hamr";
            count = 8;
         };
		};
		class TransportBackpacks
		{
		};
		class TransportMagazines
		{
         class _xx_CUP_20Rnd_762x51_B_SCAR
			{
				magazine = "CUP_20Rnd_762x51_B_SCAR";
				count = 30;
			};
         class _xx_16Rnd_9x21_Mag
         {
            magazine = "16Rnd_9x21_Mag";
            count = 16;
         };
         class _xx_CUP_200Rnd_TE4_Red_Tracer_556x45_M249
         {
            magazine = "CUP_200Rnd_TE4_Red_Tracer_556x45_M249";
            count = 4;
         };
      };
      class TransportWeapons
		{
         class _xx_CUP_arifle_Mk17_STD_black
         {
            weapon = "CUP_arifle_Mk17_STD_black";
            count = 8;
         };
         class _xx_NOR_Glock_17_S
         {
            weapon = "NOR_Glock_17_S";
            count = 8;
         };
         class _xx_CUP_lmg_m249_SQuantoon
         {
            weapon = "CUP_lmg_m249_SQuantoon";
            count = 1;
         };
		};
   };
};
