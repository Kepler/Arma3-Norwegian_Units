class DefaultEventHandlers;
class CBA_Extended_EventHandlers_base;
class CfgPatches
{
	class NOR_Randomisation
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"NOR_Core","NOR_Units","NOR_Uniforms","NOR_Insignias","cba_main"};
	};
};
class Extended_PreInit_EventHandlers
{
    class ADDON
	 {
		 init = "(call compile preProcessFileLineNumbers ""\NOR_Randomisation\initSettings.sqf"")";
	};
};
