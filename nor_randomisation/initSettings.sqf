[
   "NORUnits_random_insignias",
   "CHECKBOX",
   "Randomize insignias",
   "Norwegian Units randomization",
   true,
   true
] call CBA_settings_fnc_init;
[
   "NORUnits_random_uniforms",
   "CHECKBOX",
   "Randomize uniforms",
   "Norwegian Units randomization",
   true,
   true
] call CBA_settings_fnc_init;
[
   "NORUnits_random_vests",
   "CHECKBOX",
   "Randomize vests",
   "Norwegian Units randomization",
   true,
   true
] call CBA_settings_fnc_init;
[
   "NORUnits_random_headgear",
   "LIST",
   "Randomize headgear",
   "Norwegian Units randomization",
   [[0, 1 ,2],["Off", "Only helmets", "Helmets and caps"], 1],
   true
] call CBA_settings_fnc_init;
[
   "NORUnits_random_facewear",
   "CHECKBOX",
   "Randomize facewear",
   "Norwegian Units randomization",
   true,
   true
] call CBA_settings_fnc_init;
[
   "NORUnits_random_weapons",
   "CHECKBOX",
   "Randomize weapons",
   "Norwegian Units randomization",
   true,
   true
] call CBA_settings_fnc_init;
