if (isServer) then {
	if(isNil "NORUnits_random_weapons") then {
	   diag_log "NORUnits_random_weapons was not defined. Should have been defined in preinit";
	   NORUnits_random_weapons = false;
	};
	if(isNil "NORUnits_random_uniforms") then {
	   diag_log "NORUnits_random_uniforms was not defined. Should have been defined in preinit";
	   NORUnits_random_uniforms = false;
	};
	if(isNil "NORUnits_random_vests") then {
	   diag_log "NORUnits_random_vests was not defined. Should have been defined in preinit";
	   NORUnits_random_vests = false;
	};
	if(isNil "NORUnits_random_headgear") then {
	   diag_log "NORUnits_random_headgear was not defined. Should have been defined in preinit";
	   NORUnits_random_headgear = 0;
	};
	if(isNil "NORUnits_random_insignias") then {
	   diag_log "NORUnits_random_insignias was not defined. Should have been defined in preinit";
	   NORUnits_random_insignias = false;
	};
	if(isNil "NORUnits_random_facewear") then {
	   diag_log "NORUnits_random_facewear was not defined. Should have been defined in preinit";
	   NORUnits_random_facewear = false;
	};
	waitUntil {!isNull _this};
	_this execVM "\NOR_Randomisation\data\scripts\fsk_multi\rnd_fsk_multi.sqf"; // Weapons
	_this execVM "\NOR_Randomisation\data\scripts\fsk_multi\weapon_rnd_fsk_multi.sqf"; // Uniform, Vest, Headgear, Facewear and Insignias
	};
