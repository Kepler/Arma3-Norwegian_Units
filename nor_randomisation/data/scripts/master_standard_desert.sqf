if (isServer) then {
	waitUntil {!isNull _this};
	if(isNil "NORUnits_random_weapons") then {
	   diag_log "NORUnits_random_weapons was not defined. Should have been defined in preinit";
	   NORUnits_random_weapons = false;
	};
	if(isNil "NORUnits_random_uniforms") then {
	   diag_log "NORUnits_random_uniforms was not defined. Should have been defined in preinit";
	   NORUnits_random_uniforms = false;
	};
	if(isNil "NORUnits_random_vests") then {
	   diag_log "NORUnits_random_vests was not defined. Should have been defined in preinit";
	   NORUnits_random_vests = false;
	};
	if(isNil "NORUnits_random_headgear") then {
	   diag_log "NORUnits_random_headgear was not defined. Should have been defined in preinit";
	   NORUnits_random_headgear = 0;
	};
	if(isNil "NORUnits_random_insignias") then {
	   diag_log "NORUnits_random_insignias was not defined. Should have been defined in preinit";
	   NORUnits_random_insignias = false;
	};
	if(isNil "NORUnits_random_facewear") then {
	   diag_log "NORUnits_random_facewear was not defined. Should have been defined in preinit";
	   NORUnits_random_facewear = false;
	};
if ((uniform _this == "NOR_Uniform_Desert") or (uniform _this == "NOR_Uniform_Medic_Desert")) then {
	_this execVM "\NOR_Randomisation\data\scripts\standard_desert\uniform_rnd_standard_desert.sqf"; // Uniform Randomisation
	_this execVM "\NOR_Randomisation\data\scripts\standard_desert\insignias_standard_desert.sqf"; // Insignias
};
}; /* "if (isServer)" END - DO NOT DELETE
