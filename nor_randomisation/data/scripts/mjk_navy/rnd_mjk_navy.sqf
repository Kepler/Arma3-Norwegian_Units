
/*
Norwegian Units equip function, by Vasilyevich
this code is made for the ArmA 3 Norwegian Units modification
http://forums.bistudio.com/showthread.php?179995-Norwegian-Units
i don't support changed code based on this work
*/
if (isServer) then {
// Checks Items
// Store Items
_vestItems = vestItems _this;
_uniformItems = uniformItems _this;
// Removes Items
if(NORUnits_random_uniforms) then {
	removeUniform _this;
};
removeVest _this;
removeHeadgear _this;
// Randomize Uniform
if(NORUnits_random_uniforms) then {
	_uniform = (ceil (random 6));
	switch (_uniform ) do
	{
		case 1: {
			_this forceAddUniform "NOR_Uniform_MJK_Grey";
			};
		case 2: {
			_this forceAddUniform "NOR_Uniform_Short_MJK_Grey";
			};
		case 3: {
			_this forceAddUniform "NOR_Uniform_MJK_Coyote";
			};
		case 4: {
			_this forceAddUniform "NOR_Uniform_Short_MJK_Coyote";
			};
		case 5: {
			_this forceAddUniform "NOR_Uniform_MJK_Navy";
			};
		case 6: {
			_this forceAddUniform "NOR_Uniform_Short_MJK_Navy";
			};
	};
};
// Randomize Vest
if(NORUnits_random_vests) then {
	_vest = (ceil (random 6));
	switch (_vest ) do
	{
		case 1: {
			_this addvest "NOR_Carrier_Standard_2_Navy";
			};
		case 2: {
			_this addvest "NOR_Carrier_Standard_1_Navy";
			};
		case 3: {
			_this addvest "NOR_Carrier_Standard_2_Navy";
			};
		case 4: {
			_this addvest "NOR_Carrier_Standard_1_Navy";
			};
		case 5: {
			_this addvest "NOR_Carrier_Standard_2_Desert";
			};
		case 6: {
			_this addvest "NOR_Carrier_Standard_1_Desert";
			};
	};
};
// Randomize Headgear
if(NORUnits_random_headgear != 0) then {
	private "_maxNo";
	switch (NORUnits_random_headgear) do
	{
		case 1: { _maxNo = 9;}:
		default { _maxNo = 12;};
	};
	_helmet = (ceil (random _maxNo));
	switch (_helmet ) do
	{
		case 1: {
			_this addheadgear "NOR_Helmet_ECH_Light_Desert";
			};
		case 2: {
			_this addheadgear "NOR_Helmet_ECH_Light_Grey";
			};
		case 3: {
			_this addheadgear "NOR_Helmet_ECH_Desert";
			};
		case 4: {
			_this addheadgear "NOR_Helmet_ECH_Grey";
			};
		case 5: {
			_this addheadgear "NOR_Helmet_ECH_Light_Desert";
			};
		case 6: {
			_this addheadgear "NOR_Helmet_ECH_Light_Grey";
			};
		case 7: {
			_this addheadgear "NOR_Helmet_ECH_Desert";
			};
		case 8: {
			_this addheadgear "NOR_Helmet_ECH_Grey";
			};
		case 9: {
			_this addheadgear "NOR_Helmet_ECH_Multi";
			};
		case 10: {
			_this addheadgear "NOR_Cap_Headphones_Coyote";
			};
		case 11: {
			_this addheadgear "NOR_Cap_Headphones_Grey";
			};
		case 12: {
			_this addheadgear "NOR_Cap_Headphones_Navy";
			};
	};
};
// Randomize Facewear
if(NORUnits_random_facewear) then {
	_facewear = (ceil (random 6));
	switch (_facewear ) do
	{
		case 1: { _this addGoggles "G_Bandanna_oli"; };
		case 2: { _this addGoggles "G_Bandanna_khk"; };
		case 3: { _this addGoggles "G_Bandanna_tan"; };
		case 4: { _this addGoggles "G_Bandanna_beast"; };
		case 5: { _this addGoggles "G_Bandanna_blk"; };
		case 6: { _this addGoggles "G_Bandanna_shades"; };
	};
};
// Randomize Insignias
private "_insignia";
if(NORUnits_random_insignias) then {
	_insignia = (ceil (random 3));
} else {
	_insignia = 2;
};
switch (_insignia ) do
{
	case 1: {
		[_this,"NOR_Insignia_Flag_IR"] call BIS_fnc_setUnitInsignia;
		};
	case 2: {
		[_this,"NOR_Insignia_MJK_BW"] call BIS_fnc_setUnitInsignia;
		};
	case 3: {
		[_this,"NOR_Insignia_viking_BW"] call BIS_fnc_setUnitInsignia;
		};
};

// Read Items
if(NORUnits_random_uniforms) then {
	{_this addItemToUniform _x} forEach _uniformItems;
};
if(NORUnits_random_vests) then {
	{_this addItemToVest _x} forEach _vestItems;
};
};
